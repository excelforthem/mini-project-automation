<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Feature Login - Negative Case</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>48fe7e27-1a7d-4409-9a08-8150ce6cd169</testSuiteGuid>
   <testCaseLink>
      <guid>d7a0ab82-22ab-4eca-8ef4-51de440b7562</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG002 - User login with unregistered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f79d12ad-17bd-4bf5-be35-85499030f357</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG003_User login with email that does not exist</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1b8a09c6-8c93-4c45-ae4e-61eca3ddfb5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG004_User login with empty email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3e9f9c1b-7ffa-4585-b3c1-4618eb23d4e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG005_User login with invalid email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cb933ed5-e503-4a10-83e5-4a7b545f3500</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data_Login_Email_Invalid</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>cb933ed5-e503-4a10-83e5-4a7b545f3500</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>c504d8ca-fd7e-4da0-9f62-2a377eaaa5d3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c0d058c4-9dd9-44b1-93d0-9ce720cc6f81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG006_User login with empty password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f32dd34f-52c2-444d-9a0a-86c9cd4c8bbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG007_User login with incorrect password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
