import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Reusable/Reusable Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Keranjang Barang/input_Build it_button-2 product-box-add-to-cart-button'))

WebUI.click(findTestObject('Keranjang Barang/ck_add ons_product_home'))

WebUI.setText(findTestObject('Keranjang Barang/inp_quantity_produk_home'), '-1')

WebUI.click(findTestObject('Keranjang Barang/btn_add_product to shopping cart'))

WebUI.waitForPageLoad(5)

WebUI.verifyElementVisible(findTestObject('Keranjang Barang/spn_Quantity should be positive_home'))

WebUI.closeBrowser()

