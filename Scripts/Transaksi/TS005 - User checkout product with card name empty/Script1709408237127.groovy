import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Reusable/Reusable add to cart detail'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Shopping Cart/ck_agree_with_the_terms'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Shopping Cart/btn_Checkout'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/btn_continue_billing_address_new'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/btn_continue_shipping_address'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/radio_shipping_method_1'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/btn_continue_shipping_method'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/radio_cc'))

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/btn_continue_payment_method'))

WebUI.selectOptionByIndex(findTestObject('transaksi/Page_Demo Web Shop. Checkout/select_cc'), '0', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('transaksi/Page_Demo Web Shop. Checkout/inp_cc_number'), '4012888888881881')

WebUI.selectOptionByIndex(findTestObject('transaksi/Page_Demo Web Shop. Checkout/select_exp_day'), '3', FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByIndex(findTestObject('transaksi/Page_Demo Web Shop. Checkout/select_exp_years'), '3', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('transaksi/Page_Demo Web Shop. Checkout/inp_CardCode'), '772')

WebUI.click(findTestObject('transaksi/Page_Demo Web Shop. Checkout/btn_continue_payment_information'))

WebUI.verifyElementVisible(findTestObject('transaksi/Page_Demo Web Shop. Checkout/div_Enter cardholder name'))

WebUI.closeBrowser()

