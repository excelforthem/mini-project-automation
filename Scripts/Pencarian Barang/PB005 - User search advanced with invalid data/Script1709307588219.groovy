import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Pencarian Barang/Search/inp_search'), 'Blue Jeans')

WebUI.click(findTestObject('Object Repository/Pencarian Barang/Search/btn_search_produk'))

WebUI.click(findTestObject('Object Repository/Pencarian Barang/Advanced search/ck_advanced_search'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Pencarian Barang/Advanced search/select_category'), '5', true)

WebUI.click(findTestObject('Object Repository/Pencarian Barang/Advanced search/ck_sub_category'))

WebUI.setText(findTestObject('Object Repository/Pencarian Barang/Advanced search/inp_price_from'), '100')

WebUI.setText(findTestObject('Object Repository/Pencarian Barang/Advanced search/inp_price_to'), '500')

WebUI.click(findTestObject('Object Repository/Pencarian Barang/Advanced search/btn_search_advanced'))

WebUI.scrollToPosition(0, 500)

WebUI.waitForPageLoad(5)

WebUI.verifyElementVisible(findTestObject('Pencarian Barang/Search/spn_No products criteria'))

WebUI.closeBrowser()

