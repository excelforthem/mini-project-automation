<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>spn_Shopping Cart is empty_shopping cart</name>
   <tag></tag>
   <elementGuidId>e1fe83a1-b69d-4b62-8699-4bf52ca47142</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[2]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.order-summary-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c1381658-ef0f-405b-9b2a-1d55b0c7b424</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>order-summary-content</value>
      <webElementGuid>19735f05-b093-432c-804b-c0e43b337572</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
Your Shopping Cart is empty!    
</value>
      <webElementGuid>4a5ad04c-82c2-4ab5-999c-f27fefcf7578</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]</value>
      <webElementGuid>7fe47e6b-042f-4199-85ec-b60fd8cf86f0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[2]/following::div[2]</value>
      <webElementGuid>acc03d0e-26c6-4ad5-91c0-d01f21afb84e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete'])[1]/following::div[4]</value>
      <webElementGuid>71783ad9-9dba-47bd-bb02-75e74aa8254b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[1]</value>
      <webElementGuid>7a0a5cc4-267c-4ea2-9ec9-926474f5753d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::div[1]</value>
      <webElementGuid>c8db03e1-871d-4bd4-af62-1d443bcc985a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your Shopping Cart is empty!']/parent::*</value>
      <webElementGuid>27d7dae0-9300-4c29-a693-4b9961bad591</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/div[2]/div</value>
      <webElementGuid>9a48349d-30c0-44cd-8ea5-94d39c6270a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    
Your Shopping Cart is empty!    
' or . = '
    
    
Your Shopping Cart is empty!    
')]</value>
      <webElementGuid>76f3c426-e63f-4ab5-96f4-cf25cc0a25f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
