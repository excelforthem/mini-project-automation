<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inp_change_quantity_shopping cart</name>
   <tag></tag>
   <elementGuidId>a56b9d57-cb1a-4ed6-9b4e-3db857eac521</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='itemquantity4025202']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;itemquantity4025202&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c2f2f9e4-b847-4f97-98d6-8e01471a2e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>itemquantity4025202</value>
      <webElementGuid>65073437-d1bc-4889-81e3-128ededb806a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>739de14b-a541-44af-9b29-637e74fd29ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>95bc8ba9-0c34-40e2-893e-e2f73e8ec7f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>qty-input</value>
      <webElementGuid>81d839df-bf52-4283-a16c-284eaf08856a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/table[@class=&quot;cart&quot;]/tbody[1]/tr[@class=&quot;cart-item-row&quot;]/td[@class=&quot;qty nobr&quot;]/input[@class=&quot;qty-input&quot;]</value>
      <webElementGuid>2533eb06-652a-49eb-887f-bfa2487d5887</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='itemquantity4025202']</value>
      <webElementGuid>16035397-a2d4-4812-9720-3158a6393449</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/input</value>
      <webElementGuid>54d13671-cbdd-4b4e-a9f7-f9901cc70e7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'itemquantity4025202' and @type = 'text']</value>
      <webElementGuid>574aa442-d667-407b-a973-bc027c3da6f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
