<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>spn_No products criteria</name>
   <tag></tag>
   <elementGuidId>53aea866-df7a-48a8-af80-589e07d5942a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong.result</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>15431b22-3296-4a60-bf94-fe54a4e12ca4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>result</value>
      <webElementGuid>b8530f5c-0493-4d8e-906c-e88339a196a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    No products were found that matched your criteria.
                </value>
      <webElementGuid>d4647d75-8564-46d0-ab75-c593804f31e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/strong[@class=&quot;result&quot;]</value>
      <webElementGuid>ea676846-e4e8-44ad-bd78-a1d033c51662</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      <webElementGuid>221054cc-a3c5-4698-a522-1a6941612e66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::strong[1]</value>
      <webElementGuid>bdb1c3da-48d9-41ee-af5d-a9af3ee10255</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::strong[1]</value>
      <webElementGuid>c59cbec2-48dd-4596-829d-02795dbe7fb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='No products were found that matched your criteria.']/parent::*</value>
      <webElementGuid>81e501a5-f974-4ee8-98e3-7ca05f2fa7d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/strong</value>
      <webElementGuid>1e0282e0-2ded-47a4-9470-481aadb5ec45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                    No products were found that matched your criteria.
                ' or . = '
                    No products were found that matched your criteria.
                ')]</value>
      <webElementGuid>d13b1dcd-f7d7-4ce1-b76d-77bf6908ee1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
