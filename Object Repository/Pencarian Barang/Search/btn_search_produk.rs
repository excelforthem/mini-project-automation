<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_search_produk</name>
   <tag></tag>
   <elementGuidId>157f7721-a236-445e-9fad-3584bcb882cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.button-1.search-box-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>db1e0190-1095-46ab-bf8b-f0729ff0f0f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>58905124-f07d-4309-bfce-16de566a724a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 search-box-button</value>
      <webElementGuid>e8cb67af-a933-46c1-85d2-6a74c13df892</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Search</value>
      <webElementGuid>50e1a8af-6bd3-47fb-b22b-474b9eea341c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header&quot;]/div[@class=&quot;search-box&quot;]/form[1]/input[@class=&quot;button-1 search-box-button&quot;]</value>
      <webElementGuid>fd09fb90-9698-4eab-95f5-16bd7301c979</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Search']</value>
      <webElementGuid>38991a9f-2e06-448d-b881-780749c29224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>ee6fe08e-3e15-4b3f-b5bf-a8738c2221b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit']</value>
      <webElementGuid>0de44a93-8361-444f-99d5-77b817ed40aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
