<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>spn_Min_3_characters</name>
   <tag></tag>
   <elementGuidId>44a049b0-feb9-466b-a38a-5b26e82d322e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong.warning</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>85b3194f-f0d5-4cd3-9648-ed229afb823a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>warning</value>
      <webElementGuid>9bf5fe6a-153f-4a96-986d-fffd45391991</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Search term minimum length is 3 characters
                </value>
      <webElementGuid>518bc95d-491f-4ff0-a8c8-b01221b8b5e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/strong[@class=&quot;warning&quot;]</value>
      <webElementGuid>5e22d24c-88bd-422b-b0b4-718fc679a6b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      <webElementGuid>a1ca2f60-24f4-497d-a048-83fcfb9ad6b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::strong[1]</value>
      <webElementGuid>939b3218-6753-4255-8f1d-737ddde1531f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::strong[1]</value>
      <webElementGuid>68693f2d-8359-4c0f-96c5-a5443f1a0f3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Search term minimum length is 3 characters']/parent::*</value>
      <webElementGuid>47cd1c88-5e95-44be-b712-a204c4088d8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/strong</value>
      <webElementGuid>35212a49-5b1e-4408-9107-3b9696a06b8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                    Search term minimum length is 3 characters
                ' or . = '
                    Search term minimum length is 3 characters
                ')]</value>
      <webElementGuid>3da0a52a-e115-400b-87a3-176b9d0f751d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
