<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_card</name>
   <tag></tag>
   <elementGuidId>f431f79d-a2b2-4a66-bf4b-7df93e6dcd43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.product-item</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a9e715d7-aadc-4b6a-bdc4-676583da40b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-item</value>
      <webElementGuid>8874a2ee-37cd-46d2-ba35-050afcfdfc5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-productid</name>
      <type>Main</type>
      <value>36</value>
      <webElementGuid>1e20a6e5-0d11-4046-82c6-e6f1bbab67a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
            Blue Jeans
        
            
                
                    
                    
                
            
        
            Jeans
        
        
            
                1.00
            
            
                
                    
            
            
        
    
</value>
      <webElementGuid>8434f559-3a95-4db2-915a-5c17d80d9ab5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]</value>
      <webElementGuid>9f39f4c1-bc63-4407-8f5b-df4c1d9aa579</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[4]</value>
      <webElementGuid>035fa854-8e09-49d6-8750-714c7f712881</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[4]</value>
      <webElementGuid>e057bc48-1366-4823-88dd-738f8b8935e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div</value>
      <webElementGuid>6162bdb1-2830-443b-8088-fb6a20780c38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
        
            
        
    
    
        
            Blue Jeans
        
            
                
                    
                    
                
            
        
            Jeans
        
        
            
                1.00
            
            
                
                    
            
            
        
    
' or . = '
    
        
            
        
    
    
        
            Blue Jeans
        
            
                
                    
                    
                
            
        
            Jeans
        
        
            
                1.00
            
            
                
                    
            
            
        
    
')]</value>
      <webElementGuid>d904d306-d1bf-4e0e-bfb3-b8faff32855c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
