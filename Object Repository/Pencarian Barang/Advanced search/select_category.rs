<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_category</name>
   <tag></tag>
   <elementGuidId>641afb71-eab6-472c-87e1-d6e4b711afab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Cid']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Cid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>3a33b253-4018-44c6-a842-1d7141eb16d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>8d8742fb-8613-4cd7-803c-9ffca9eaae3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-number</name>
      <type>Main</type>
      <value>The field Category must be a number.</value>
      <webElementGuid>07461a33-bb19-48d0-9ead-e967e8ecb195</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>3a2c0736-eb35-4bcc-b8f4-e044ffec83fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>99234feb-18e7-43d0-85c9-ec80b64c121d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
</value>
      <webElementGuid>4852afca-aa49-4583-b388-b088f23ce8e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Cid&quot;)</value>
      <webElementGuid>182428d0-a09a-4f6b-bfd0-36cf38f96179</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Cid']</value>
      <webElementGuid>c0b6bd22-f676-490d-be01-6a00c2e63d60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div/select</value>
      <webElementGuid>5594579b-1969-4a4d-9916-94916a4ef644</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category:'])[1]/following::select[1]</value>
      <webElementGuid>553aea74-94a6-4268-af76-376bd228ec3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced search'])[1]/following::select[1]</value>
      <webElementGuid>14129d0c-4450-4656-90b3-cfd8a62ee397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automatically search sub categories'])[1]/preceding::select[1]</value>
      <webElementGuid>c8d3c62e-74db-48b2-a0cc-15a80ff7e057</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manufacturer:'])[1]/preceding::select[1]</value>
      <webElementGuid>bbbdca35-01bf-40b0-9054-89dea20da70f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>c806bb84-7457-4e01-996d-506571e15fc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'Cid' and @name = 'Cid' and (text() = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
' or . = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
')]</value>
      <webElementGuid>a08d3bf0-1444-472e-a5c5-6913edf78455</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
