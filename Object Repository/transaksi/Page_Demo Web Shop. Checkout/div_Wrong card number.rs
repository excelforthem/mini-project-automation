<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Wrong card number</name>
   <tag></tag>
   <elementGuidId>6d6a90e7-4005-421f-88ef-5a7b1468924a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/following::li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > ul > li:nth-of-type(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>cc7e3717-b618-42f8-b0df-b311e445e69c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Wrong card number</value>
      <webElementGuid>0639665d-9eb6-41c7-a850-328a2b6d736e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/ul[1]/li[2]</value>
      <webElementGuid>491fbb48-2d6e-4559-abe4-35032ee498a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div[2]/div/ul/li[2]</value>
      <webElementGuid>34be41a1-c328-4579-809e-4aeb61a5ec5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter cardholder name'])[1]/following::li[1]</value>
      <webElementGuid>b64076e7-0836-4cee-8749-271f3250a460</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/following::li[2]</value>
      <webElementGuid>c140f20e-fd8c-4f66-a631-db97f7fa61c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wrong card code'])[1]/preceding::li[1]</value>
      <webElementGuid>c8bc0b60-2378-41f9-923e-600ed465198a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[4]/preceding::li[2]</value>
      <webElementGuid>569cc7a2-3887-47f0-a746-59e4e2cb5a71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Wrong card number']/parent::*</value>
      <webElementGuid>9eb3578d-152e-490d-a0d8-3e6c8eb665b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div[2]/div/ul/li[2]</value>
      <webElementGuid>74861984-dad2-4b86-94c7-1b1da45fa116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Wrong card number' or . = 'Wrong card number')]</value>
      <webElementGuid>094624d3-80ff-4bde-81b3-5a2e1dfb2c0f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
