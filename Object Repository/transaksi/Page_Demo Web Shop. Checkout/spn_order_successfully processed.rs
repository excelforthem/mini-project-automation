<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>spn_order_successfully processed</name>
   <tag></tag>
   <elementGuidId>2c41468c-91f6-42b7-89de-78f069d0c44b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>622841d1-d7e0-4c26-8826-99e1d41982af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your order has been successfully processed!</value>
      <webElementGuid>09ac2cc2-0208-4b70-946f-ce1836814c53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]/div[@class=&quot;title&quot;]/strong[1]</value>
      <webElementGuid>30ae8ac8-dea5-4d63-a2ca-e86735930842</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::strong[1]</value>
      <webElementGuid>9fd590ef-0616-4683-87da-f01938203f63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gift Cards'])[2]/following::strong[1]</value>
      <webElementGuid>68ceddf4-df4e-4cb6-97ee-b4c7b20909fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order number: 1630042'])[1]/preceding::strong[1]</value>
      <webElementGuid>cf304e12-e2f9-455a-8ffc-f8924f08ccf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Click here for order details.'])[1]/preceding::strong[1]</value>
      <webElementGuid>be4fbb27-cdea-4041-8ec2-4c0325cce378</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your order has been successfully processed!']/parent::*</value>
      <webElementGuid>3b4ed624-fb65-4cca-a04d-bb26e1fc0d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>077a8f1b-836f-4755-a231-0c099122428a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Your order has been successfully processed!' or . = 'Your order has been successfully processed!')]</value>
      <webElementGuid>8897e14e-fbe9-4b9b-8fa2-f2618f50dcf0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
