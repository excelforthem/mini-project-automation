<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_continue_payment_information</name>
   <tag></tag>
   <elementGuidId>9cde0589-4ce5-4ca8-ac57-f7c7173fc86c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value='Continue'])[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.button-1.payment-info-next-step-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>48dd5dd7-2291-4bc8-962d-9cde4dcbc4fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>10364a73-3f69-4be1-b2cd-6171b45b3fac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-1 payment-info-next-step-button</value>
      <webElementGuid>7bc98326-ea79-49a5-ab65-5d34fa3630e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>PaymentInfo.save()</value>
      <webElementGuid>b7508783-5a7b-4b35-821e-e63241cfc514</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>b574b161-62fe-4a21-af08-d9ed2b191d2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;payment-info-buttons-container&quot;)/input[@class=&quot;button-1 payment-info-next-step-button&quot;]</value>
      <webElementGuid>a7ed2f71-acb2-49c6-a2dc-018d993a97e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value='Continue'])[5]</value>
      <webElementGuid>a04766a4-d08c-46f6-8a18-5ec38ef86fa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='payment-info-buttons-container']/input</value>
      <webElementGuid>76f4588a-1e06-40d3-a453-e53d59ec29c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/div/input</value>
      <webElementGuid>9dc76bf4-561f-4978-ab0c-d94e35431e3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button']</value>
      <webElementGuid>9f091e5d-6b1e-4147-b7c9-a8e729c2483c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
